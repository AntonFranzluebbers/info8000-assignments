from flask import Flask, escape, request, jsonify
import sqlite3

read_key = '9e895614-6e70-4552-b43a-a058d71bbe4c'
write_key = 'd713b33d-8b18-49a1-931e-f3a47621459b'

app = Flask(__name__)

@app.route('/')
def fun():
    return "Specify a route you dummy"


def connectToDB(request):
    vers = request.headers.get("version")
    if vers is not None:
        c = sqlite3.connect("echo_data_" + str(vers) + ".db")
    else:
        c = sqlite3.connect("echo_data.db")
    return c



# GET Data

@app.route('/dev')
def testingCalls():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    player_name = request.args.get("name")

    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = """
    SELECT * FROM Players
    
    """
    data = (player_name,)
    curr.execute(query,data)
    result = [dict(row) for row in curr.fetchall()]
    if (len(result) > 0):
        c.close()
        return jsonify(result[0])
    else:
        c.close()
        return "Player not found"

@app.route('/player')
def getPlayerData():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    player_name = request.args.get("name")
    if player_name is None:
        return "Must specify `name` of player"
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = "SELECT * FROM Players WHERE player_name=?"
    data = (player_name,)
    curr.execute(query,data)
    result = [dict(row) for row in curr.fetchall()]
    if (len(result) > 0):
        c.close()
        return jsonify(result[0])
    else:
        c.close()
        return "Player not found"


@app.route('/match')
def getMatchData():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    session_id = request.args.get("session_id")
    match_time = request.args.get("match_time")
    if session_id is None or match_time is None:
        return "Must specify `session_id` and `match_time`"
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    query = "SELECT * FROM Matches WHERE session_id=? AND match_time=?"
    data = (session_id,match_time)
    curr = c.cursor()
    try:
        curr.execute(query,data)
    except:
        return "Get match failed"
    result = [dict(row) for row in curr.fetchall()]
    c.close()
    return jsonify(result)


@app.route('/allMatches')
def getAllMatches():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.execute("SELECT * FROM Matches")
    result = [dict(row) for row in curr.fetchall()]
    c.close()
    return jsonify(result)

@app.route('/allPlayerData')
def getAllPlayerData():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.execute("SELECT * FROM Players")
    result = [dict(row) for row in curr.fetchall()]
    c.close()
    return jsonify(result)

@app.route('/playerCount')
def getPlayerCount():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    curr = c.execute("SELECT COUNT(*) FROM Players")
    out = str(curr.fetchall()[0][0])
    c.close()
    return out

@app.route('/getPlayerList', methods=['GET'])
def getPlayerList():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    curr = c.execute("SELECT player_name FROM Players")
    out = str(curr.fetchall())
    c.close()
    return out

@app.route('/event', methods=['GET'])
def getEvent():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    session_id = request.args.get("session_id")
    player_name = request.args.get("player_name")
    c.row_factory = sqlite3.Row
    if (session_id is not None):
        curr = c.cursor()
        query = "SELECT * FROM Events WHERE session_id=?"
        data = (session_id,)
        curr.execute(query,data)
        result = [dict(row) for row in curr.fetchall()]
        c.close()
        return jsonify(result)
    elif (player_name is not None):
        curr = c.cursor()
        query = "SELECT * FROM Events WHERE player_name=?"
        data = (player_name,)
        curr.execute(query,data)
        result = [dict(row) for row in curr.fetchall()]
        c.close()
        return jsonify(result)
    else:
        c.close()
        return ''



# Add Data

@app.route('/addPlayer', methods=['POST'])
def addPlayer():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    if request.values.get('player_name') is None or request.values.get('player_id') is None:
        return "Must specify `player_name` and `player_id`"
    c = connectToDB(request)
    query = "INSERT OR IGNORE INTO Players (player_id, player_name) VALUES(?,?)"
    data = (request.values.get("player_id"), request.values.get("player_name"))
    curr = c.cursor()
    # try:
    curr.execute(query, data)
    # except:
    #     c.close()
    #     return "Failed to insert player"
    c.commit()
    c.close()
    return "Player successfully inserted"

@app.route('/accumulatePlayer', methods=['POST'])
def accumulatePlayer():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    if request.values.get('player_name') is None:
        return "Must specify `player_name`"
    c = connectToDB(request)
    query = """
    UPDATE Players 
    SET 
        possession_time = possession_time + ?, 
        total_assists = total_assists + ?, 
        total_blocks = total_blocks + ?, 
        total_catches = total_catches + ?, 
        total_goals = total_goals + ?,
        `2_pointers` = `2_pointers` + ?,
        `3_pointers` = `3_pointers` + ?,
        total_interceptions = total_interceptions + ?, 
        total_passes = total_passes + ?, 
        total_points = total_points + ?, 
        total_saves = total_saves + ?, 
        total_shots_taken = total_shots_taken + ?, 
        total_steals = total_steals + ?, 
        total_stuns= total_stuns + ?, 
        total_wins = total_wins + ?, 
        game_count= game_count + 1
    WHERE player_name=?"""

    data = (
        request.values.get('possession_time',0),
        request.values.get('assists',0),
        request.values.get('blocks',0),
        request.values.get('catches',0),
        request.values.get('goals',0),
        request.values.get('2_pointers',0),
        request.values.get('3_pointers',0),
        request.values.get('interceptions',0),
        request.values.get('passes',0),
        request.values.get('points',0),
        request.values.get('saves',0),
        request.values.get('shots_taken',0),
        request.values.get('steals',0),
        request.values.get('stuns',0),
        request.values.get('wins',0),
        request.values.get('player_name', ""))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        return "Failed to update Player"
    c.commit()
    c.close()
    return "Player updated"


@app.route('/addMatch', methods=['POST'])
def addMatch():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    if session_id is None or match_time is None:
        return "Must specify `session_id` and `match_time`"
    c = connectToDB(request)
    query = """
    REPLACE INTO Matches (
        session_id,
        match_time,
        round,
        private,
        winning_team,
        blue_team_name,
        orange_team_name,
        blue_team_score,
        orange_team_score)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)"""

    data = (
        session_id,
        match_time,
        request.values.get('round', 0),
        request.values.get('private', 0),
        request.values.get('winning_team', ""),
        request.values.get('blue_team_name', ""),
        request.values.get('orange_team_name', ""),
        request.values.get('blue_team_score', 0),
        request.values.get('orange_team_score', 0))

    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        return "Failed to add Match"
    c.commit()
    c.close()
    return "Match added"



@app.route('/addEvent', methods=['POST'])
def addEvent():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    event_type = request.values.get('event_type')
    player_name = request.values.get('player_name')
    if session_id is None or match_time is None or event_type is None or player_name is None:
        return "Event must specify `session_id` and `match_time` and `event_type` and `player_name`"
    c = connectToDB(request)
    query = """
    INSERT INTO Events (
        session_id,
        match_time, 
        event_type,
        player_name,
        event_time,
        other_player_name,
        pos_x,
        pos_y,
        pos_z)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)
    """

    data = (
        session_id,
        match_time,
        event_type,
        player_name,
        request.values.get('event_time', 0),
        request.values.get('other_player_name', ""),
        request.values.get('pos_x', 0),
        request.values.get('pos_y', 0),
        request.values.get('pos_z', 0))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        return "Failed to add Event"
    c.commit()
    c.close()
    return "Event added"



@app.route('/addGoal', methods=['POST'])
def addGoal():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    point_value = request.values.get('point_value')
    if session_id is None or match_time is None or point_value is None:
        return "Goal must specify `session_id` and `match_time` and `point_value`"
    c = connectToDB(request)
    query = """
    INSERT INTO Goals (
        session_id,
        match_time,
        scorer_name,
        point_value,
        disc_speed,
        goal_distance,
        assist_name,
        pos_x,
        pos_y,
        pos_z,
        trajectory)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    """

    data = (
        session_id,
        match_time,
        request.values.get('scorer_name',""),
        point_value,
        request.values.get('disc_speed',0),
        request.values.get('goal_distance',0),
        request.values.get('assist_name',""),
        request.values.get('pos_x',0),
        request.values.get('pos_y',0),
        request.values.get('pos_z',0),
        request.values.get('trajectory',""))
    curr = c.cursor()
    # try:
    curr.execute(query, data)
    # except:
    #     c.close()
    #     return "Failed to add Goal"
    c.commit()
    c.close()
    return "Goal added"


@app.route('/addMatchPlayer', methods=['POST'])
def addMatchPlayer():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    player_name = request.values.get('player_name')
    if session_id is None or match_time is None or player_name is None:
        return "MatchPlayer must specify `session_id` and `match_time` and `player_name`"
    c = connectToDB(request)
    query = """
    REPLACE INTO MatchPlayer (
        session_id,
        match_time,
        team_color,
        player_name,
        possession_time,
        points,
        `2_pointers`,
        `3_pointers`,
        shots_taken,
        saves,
        goals,
        stuns,
        passes,
        catches,
        steals,
        blocks,
        interceptions,
        assists,
        average_speed)
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""

    data = (
        session_id,
        match_time,
        request.values.get('team_color',""),
        player_name,
        request.values.get('possession_time',0),
        request.values.get('points',0),
        request.values.get('2_pointers',0),
        request.values.get('3_pointers',0),
        request.values.get('shots_taken',0),
        request.values.get('saves',0),
        request.values.get('goals',0),
        request.values.get('stuns',0),
        request.values.get('passes',0),
        request.values.get('catches',0),
        request.values.get('steals',0),
        request.values.get('blocks',0),
        request.values.get('interceptions',0),
        request.values.get('assists',0),
        request.values.get('average_speed',0))
    curr = c.cursor()
    # try:
    curr.execute(query, data)
    # except:
    #     c.close()
    #     return "Failed to add MatchPlayer"
    c.commit()
    c.close()
    return "MatchPlayer added"
