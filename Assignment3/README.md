# INFO 8000 Assignment 3
*Anton Franzluebbers*  
*2020-02-11*  

This API is intended as a way to record important match and player information from Echo Arena matches. The data is intended to be added by another standalone application, which provides the POSTs.  

## Files
 - `CreateDB.py` - Python script to create the database file
 - `EchoServer.py` - The main Flask server
 - `echo_data.db` - The sqlite3 database file
 - `README.md` - Where am I?

## API Key access
 All requests are protected by API keys. POST requests require the write key, and GET requests require either the read or write key.  
 - Read key - `9e895614-6e70-4552-b43a-a058d71bbe4c`  
 - Write key - `d713b33d-8b18-49a1-931e-f3a47621459b`  

## Example GET requests
*These require the read or write key*

### Players
 - `http://ntsfranz.crabdance.com/player?name=TEST`
    - Gets a player with name=?. Must specify a player name.

 - `http://ntsfranz.crabdance.com/getPlayerList`
    - Gets the full list of player names.

 - `http://ntsfranz.crabdance.com/playerCount`
    - Gets the count of players in the database.

### Matches

 - `http://ntsfranz.crabdance.com/match?session_id=d1234&match_time=2021-02-09 01:23:45`
    - Gets a specific match. Must supply the `session_id` and `match_time`.

 - `http://ntsfranz.crabdance.com/allMatches`
    - Gets all matches.

### Events

 - `http://ntsfranz.crabdance.com/event?player_name=TEST`
    - Gets all events performed by a specific player.

 - `http://ntsfranz.crabdance.com/event?session_id=d1234`
    - Gets all events performed within a certain session_id.



## Example POST requests
*These require the write key*

### Players

 - `http://ntsfranz.crabdance.com/addPlayer?name=TEST&player_id=123456789`
    - Adds a player with a name and player id. The other values are set to the defaults.

 - `http://ntsfranz.crabdance.com/accumulatePlayer?player_name=TEST&passes=100&points=10&2_pointers=2`
    - Updates a player with the specified values. There are a lot of parameters than can be specified, but `player_name` is required. The reason this is used instead of using the addPlayer route is because this route adds the current values to the previous player values, while incrementing the game count of that player.

### Matches

 - `http://ntsfranz.crabdance.com/addMatch?session_id=d1234&match_time=2021-02-09 01:23:45`
    - Add a match. Required parameters are `session_id` and `match_time`. There are plenty of optional parameters to set the match details.

### Events

 - `http://ntsfranz.crabdance.com/addEvent?session_id=d1234&match_time=2021-02-09 01:23:45&event_type=stun&player_name=TEST`
    - Adds an event to the table. Required parameters are `session_id` and `match_time` and `event_type` and `player_name`. There are plenty of other optional parameters.
