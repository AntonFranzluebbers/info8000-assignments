from flask import Flask, escape, request, jsonify

app = Flask(__name__)

@app.route('/')
def fun():
    name = request.args.get("name", "World")
    return jsonify({"name": name})

@app.route('/player')
def getPlayerData():
    c = sqlite3.connect("echo_data.db")
    player_name = request.args.get("name")
    curr = c.execute("SELECT * FROM Players WHERE player_name="+str(player_name))
    return curr.fetchall()
    
