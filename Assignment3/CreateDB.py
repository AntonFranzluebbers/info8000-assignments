import sqlite3
import pandas as pd

c = sqlite3.connect("echo_data.db")
try:
    
    
    c.executescript('''
    DROP TABLE IF EXISTS Events;
    DROP TABLE IF EXISTS Goals;
    DROP TABLE IF EXISTS MatchPlayer;
    DROP TABLE IF EXISTS Matches;
    DROP TABLE IF EXISTS Players;
    ''')
    
    #CREATE DATABASE
    c.executescript('''
    CREATE TABLE `Events` (
      `session_id` TEXT NOT NULL,
      `match_time` datetime NOT NULL,
      `event_type` TEXT NOT NULL,
      `player_name` TEXT NOT NULL,
      `event_time` float NOT NULL,
      `other_player_name` TEXT DEFAULT NULL,
      `pos_x` float NOT NULL,
      `pos_y` float NOT NULL,
      `pos_z` float NOT NULL,
      FOREIGN KEY (session_id, match_time) REFERENCES Matches (session_id, match_time),
      FOREIGN KEY (player_name) REFERENCES Players (player_name)
    );

    CREATE TABLE `Goals` (
      `session_id` TEXT NOT NULL,
      `match_time` datetime NOT NULL,
      `scorer_name` TEXT NOT NULL,
      `point_value` int(11) NOT NULL,
      `disc_speed` float NOT NULL,
      `goal_distance` float NOT NULL,
      `assist_name` varchar(100) DEFAULT NULL,
      `pos_x` float NOT NULL,
      `pos_y` float NOT NULL,
      `pos_z` float NOT NULL,
      `trajectory` TEXT NOT NULL,
      FOREIGN KEY (session_id, match_time) REFERENCES Matches (session_id, match_time),
      FOREIGN KEY (scorer_name) REFERENCES Players (player_name)
    );

    CREATE TABLE `Matches` (
      `session_id` TEXT NOT NULL,
      `match_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `round` int(11) NOT NULL DEFAULT '1',
      `private` tinyint(1) NOT NULL DEFAULT '0',
      `blue_team_name` TEXT NOT NULL,
      `orange_team_name` TEXT NOT NULL,
      `blue_team_score` int(11) NOT NULL DEFAULT '0',
      `orange_team_score` int(11) NOT NULL DEFAULT '0',
      `winning_team` TEXT NOT NULL,
      PRIMARY KEY(session_id, match_time)
    );

    CREATE TABLE `MatchPlayer` (
      `session_id` TEXT NOT NULL,
      `match_time` datetime NOT NULL,
      `player_name` TEXT NOT NULL,
      `team_color` TEXT NOT NULL,
      `possession_time` float NOT NULL DEFAULT '0',
      `points` int(11) NOT NULL DEFAULT '0',
      `2_pointers` int(11) NOT NULL DEFAULT '0',
      `3_pointers` int(11) NOT NULL DEFAULT '0',
      `shots_taken` int(11) NOT NULL DEFAULT '0',
      `saves` int(11) NOT NULL DEFAULT '0',
      `goals` int(11) NOT NULL DEFAULT '0',
      `stuns` int(11) NOT NULL DEFAULT '0',
      `passes` int(11) NOT NULL DEFAULT '0',
      `catches` int(11) NOT NULL DEFAULT '0',
      `steals` int(11) NOT NULL DEFAULT '0',
      `blocks` int(11) NOT NULL DEFAULT '0',
      `interceptions` int(11) NOT NULL DEFAULT '0',
      `assists` int(11) NOT NULL DEFAULT '0',
      `average_speed` float NOT NULL DEFAULT '0',
      FOREIGN KEY (session_id, match_time) REFERENCES Matches (session_id, match_time),
      FOREIGN KEY (player_name) REFERENCES Players (player_name)
    );

    CREATE TABLE `Players` (
      `player_id` bigint(200) NOT NULL,
      `profile_link` varchar(1000) NOT NULL DEFAULT 'http://ignitevr.gg/wp-content/uploads/2019/04/defualt_profile_14_4k.jpg',
      `player_name` TEXT NOT NULL DEFAULT '???',
      `possession_time` float NOT NULL DEFAULT '0',
      `total_points` int(11) NOT NULL DEFAULT '0',
      `total_shots_taken` int(11) NOT NULL DEFAULT '0',
      `total_saves` int(11) NOT NULL DEFAULT '0',
      `total_goals` int(11) NOT NULL DEFAULT '0',
      `2_pointers` int(11) NOT NULL DEFAULT '0',
      `3_pointers` int(11) NOT NULL DEFAULT '0',
      `total_stuns` int(11) NOT NULL DEFAULT '0',
      `total_passes` int(11) NOT NULL DEFAULT '0',
      `total_catches` int(11) NOT NULL DEFAULT '0',
      `total_steals` int(11) NOT NULL DEFAULT '0',
      `total_blocks` int(11) NOT NULL DEFAULT '0',
      `total_interceptions` int(11) NOT NULL DEFAULT '0',
      `total_assists` int(11) NOT NULL DEFAULT '0',
      `total_wins` int(11) NOT NULL DEFAULT '0',
      `game_count` int(11) NOT NULL DEFAULT '0',
      PRIMARY KEY (player_name)
    );

    ''')
    
    
    
    c.executescript("PRAGMA foreign_keys = ON;")
        
    
except sqlite3.Error as e:
        print(e)
finally:
    c.close()
