﻿using EchoVRDataCollector.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace EchoVRDataCollector
{
	class EVRDCApplicationContext : ApplicationContext
	{

		public static bool running = true;

		static List<string> dataCache = new List<string>();

		static DateTime lastDataTime;

		// Configuration
		private static int targetDeltaTimeMillis = 16;
		static string saveFolder = "none";
		static string fileName;
		static bool useCompression = true;
		static bool batchWrites = false;
		static bool startOnBoot = false;
		static bool restartOnCrash = false;

		public static bool UseCompression {
			get => useCompression; 
			set {
				useCompression = value;
				Settings.Default.useCompression = useCompression;
				Settings.Default.Save();
			}
		}

		public static bool BatchWrites {
			get => batchWrites; 
			set {
				batchWrites = value;
				Settings.Default.batchWrites = batchWrites;
				Settings.Default.Save();
			}
		}
		public static bool StartOnBoot {
			get => startOnBoot; 
			set {
				startOnBoot = value;
				Settings.Default.startOnBoot = startOnBoot;
				Settings.Default.Save();
			}
		}

		public static string SaveFolder {
			get => saveFolder; 
			set {
				saveFolder = value;
				Settings.Default.saveFolder = saveFolder;
				Settings.Default.Save();
			}
		}
		public static int TargetDeltaTimeMillis {
			get => targetDeltaTimeMillis; 
			set {
				targetDeltaTimeMillis = value;
				Settings.Default.targetDeltaTimeMillis = targetDeltaTimeMillis;
				Settings.Default.Save();
			}
		}

		static bool RestartOnCrash {
			get => restartOnCrash;
			set {
				restartOnCrash = value;
				Settings.Default.restartOnCrash = restartOnCrash;
				Settings.Default.Save();
			}
		}

		NotifyIcon trayIcon;

		MenuItem speedSlow;
		MenuItem speedMed;
		MenuItem speedFast;

		MenuItem setStorageLocation;

		public EVRDCApplicationContext()
		{
			ReadSettings();
			NewFilename();

			Thread mainThread = new Thread(new ThreadStart(MainThread));
			mainThread.Start();

			MenuItem startNewFile = new MenuItem("Split File", new EventHandler(NewFilenameEvent));
			MenuItem batchWritesItem = new MenuItem("Batch Writes", new EventHandler(BatchWritesEvent));
			MenuItem compressionMenuItem = new MenuItem("Use Compression", new EventHandler(UseCompressionEvent));
			MenuItem startOnBoot = new MenuItem("Start with Windows", new EventHandler(StartWithWindowsEvent));
			MenuItem restartOnCrashMenu = new MenuItem("Auto-Restart", new EventHandler(RestartOnCrashEvent));

			speedSlow = new MenuItem("Slow (1 Hz)", new EventHandler(SpeedChangeSlow));
			speedMed = new MenuItem("Med (10 Hz)", new EventHandler(SpeedChangeMed));
			speedFast = new MenuItem("Fast (60 Hz)", new EventHandler(SpeedChangeFast));

			speedSlow.RadioCheck = true;
			speedMed.RadioCheck = true;
			speedFast.RadioCheck = true;

			setStorageLocation = new MenuItem("Storage Location...", new EventHandler(SetStorageLocation));

			// Initialize Tray Icon
			trayIcon = new NotifyIcon()
			{
				Icon = Resources.AppIcon,
				ContextMenu = new ContextMenu(new MenuItem[] {
					startNewFile,
					new MenuItem("-"),
					new MenuItem("Rate", new MenuItem[] {speedSlow, speedMed, speedFast}),
					setStorageLocation,
					batchWritesItem,
					compressionMenuItem,
					restartOnCrashMenu,
					startOnBoot,
					new MenuItem("-"),
					new MenuItem("Exit", Exit)
				}),
				Visible = true
			};

			batchWritesItem.Checked = BatchWrites;
			compressionMenuItem.Checked = UseCompression;
			startOnBoot.Checked = StartOnBoot;
			restartOnCrashMenu.Checked = RestartOnCrash;
			SetStartWithWindows(StartOnBoot);

			speedSlow.Checked = false;
			speedMed.Checked = false;
			speedFast.Checked = false;
			switch (TargetDeltaTimeMillis)
			{
				case 1000:
					speedSlow.Checked = true;
					break;
				case 100:
					speedMed.Checked = true;
					break;
				case 16:
					speedFast.Checked = true;
					break;
			}

			if (SaveFolder == "none")
			{
				SetStorageLocation(null, null);
			}
			else
			{
				setStorageLocation.Text = SaveFolder;
			}
		}

		#region Event Listeners

		private void NewFilenameEvent(object sender, EventArgs e)
		{
			NewFilename();
		}

		void ReadSettings()
		{
			useCompression = Settings.Default.useCompression;
			batchWrites = Settings.Default.batchWrites;
			targetDeltaTimeMillis = Settings.Default.targetDeltaTimeMillis;
			saveFolder = Settings.Default.saveFolder;
			startOnBoot = Settings.Default.startOnBoot;
			restartOnCrash = Settings.Default.restartOnCrash;
		}

		private void SetStorageLocation(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				SaveFolder = folderBrowserDialog.SelectedPath;
				setStorageLocation.Text = SaveFolder;
			}
		}

		private void SpeedChangeSlow(object sender, EventArgs e)
		{
			SpeedChange(0);
		}
		private void SpeedChangeMed(object sender, EventArgs e)
		{
			SpeedChange(1);
		}

		private void SpeedChangeFast(object sender, EventArgs e)
		{
			SpeedChange(2);
		}

		private void SpeedChange(int speed)
		{
			speedSlow.Checked = false;
			speedMed.Checked = false;
			speedFast.Checked = false;

			switch (speed)
			{
				case 0:
					TargetDeltaTimeMillis = 1000;
					speedSlow.Checked = true;
					break;
				case 1:
					TargetDeltaTimeMillis = 100;
					speedMed.Checked = true;
					break;
				case 2:
					TargetDeltaTimeMillis = 16;
					speedFast.Checked = true;
					break;
				default:
					break;
			}
		}

		void RestartOnCrashEvent(object sender, EventArgs e)
		{
			RestartOnCrash = !RestartOnCrash;

			((MenuItem)sender).Checked = RestartOnCrash;
		}

		private void StartWithWindowsEvent(object sender, EventArgs e)
		{
			StartOnBoot = !StartOnBoot;
			SetStartWithWindows(StartOnBoot);

			((MenuItem)sender).Checked = StartOnBoot;
		}

		private static void SetStartWithWindows(bool val)
		{
			RegistryKey rk = Registry.CurrentUser.OpenSubKey
						("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

			if (val)
				rk.SetValue(Resources.AppName, Application.ExecutablePath);
			else
				rk.DeleteValue(Resources.AppName, false);
		}

		private void UseCompressionEvent(object sender, EventArgs e)
		{
			UseCompression = !UseCompression;
			((MenuItem)sender).Checked = UseCompression;
		}

		private void BatchWritesEvent(object sender, EventArgs e)
		{
			BatchWrites = !BatchWrites;
			((MenuItem)sender).Checked = BatchWrites;
		}

		void Exit(object sender, EventArgs e)
		{
			// We must manually tidy up and remove the icon before we exit.
			// Otherwise it will be left behind until the user mouses over.
			trayIcon.Visible = false;
			running = false;
			Application.Exit();
		}

		#endregion

		/// <summary>
		/// Generates a new filename from the current time.
		/// </summary>
		static void NewFilename()
		{
			// compress the file
			if (UseCompression)
			{
				if (File.Exists(Path.Combine(SaveFolder, fileName + ".json")))
				{
					string tempDir = Path.Combine(SaveFolder, "temp_zip").ToString();
					Directory.CreateDirectory(tempDir);
					File.Move(Path.Combine(SaveFolder, fileName + ".json"), Path.Combine(SaveFolder, "temp_zip", fileName + ".json"));
					ZipFile.CreateFromDirectory(tempDir, Path.Combine(SaveFolder, fileName + ".zip"));
					Directory.Delete(tempDir, true);
				}
			}

			fileName = DateTime.Now.ToString("rec_yyyy-MM-dd_HH-mm-ss");
		}

		/// <summary>
		/// Thread that times the get requests
		/// </summary>
		void MainThread()
		{
			lastDataTime = DateTime.Now;

			// Session pull loop.
			while (running)
			{
				try
				{
					GetData();
				}
				catch (Exception ex)
				{
					Console.WriteLine("Big oopsie. Please catch inside. " + ex);
				}

				Thread.Sleep(TargetDeltaTimeMillis);
			}

			NewFilename();
		}

		/// <summary>
		/// Grabs and returns data stream from EchoVR.
		/// </summary>
		void GetData()
		{
			WebResponse response;
			StreamReader sReader;

			// Create Session.
			WebRequest request = WebRequest.Create("http://127.0.0.1/session");

			// Do we get a response?
			try
			{
				response = request.GetResponse();
				lastDataTime = DateTime.Now;
			}
			catch (WebException)
			{
				// Don't update so quick if we aren't in a match anyway
				Thread.Sleep(100);
				Console.WriteLine("Not in Match");
				NewFilename();


				if (RestartOnCrash)	// TODO *and* if was spectating before
				{
					// If minutes have passed, restart EchoVR
					if (DateTime.Compare(lastDataTime.AddMinutes(5), DateTime.Now) < 0)
					{
						// Get process name
						var process = Process.GetProcessesByName("echovr");

						if (process.Length > 0)
						{
							var echo_ = process[0];
							// Get process path
							var path = echo_.MainModule.FileName;
							// close client
							echo_.Kill();
							// restart client
							Process.Start(path, "-spectatorstream");
						}
						else
						{
							Console.WriteLine("Couldn't restart EchoVR because it isn't running");
						}

						lastDataTime = DateTime.Now;
					}
				}

				return;
			}

			Stream dataStream = response.GetResponseStream();
			sReader = new StreamReader(dataStream);

			// Session Contents
			string rawJSON = sReader.ReadToEnd();



			WriteToFile(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "\t" + rawJSON);

			// pls close (;-;)
			if (sReader != null)
				sReader.Close();
			if (response != null)
				response.Close();

		}

		/// <summary>
		/// Writes the data to the file
		/// </summary>
		/// <param name="data">The data to write</param>
		void WriteToFile(string data)
		{
			if (batchWrites)
			{
				dataCache.Add(data);

				// if the time elapse since last write is less than cutoff
				if (dataCache.Count * TargetDeltaTimeMillis < 5000)
				{
					return;
				}
			}


			string filePath, directoryPath;

			// Fail if the folder doesn't even exist
			if (!Directory.Exists(SaveFolder))
			{
				SetStorageLocation(null, null);
				return;
			}

			// could combine with some other data path, such as AppData
			directoryPath = SaveFolder;

			filePath = Path.Combine(directoryPath, fileName + ".json");

			StreamWriter streamWriter = new StreamWriter(filePath, true);

			if (batchWrites)
			{
				foreach (var row in dataCache)
				{
					Console.WriteLine(row);
					streamWriter.WriteLine(row);
				}
				dataCache.Clear();
			}
			else
			{
				Console.WriteLine(data);
				streamWriter.WriteLine(data);
			}

			streamWriter.Close();
		}
	}
}