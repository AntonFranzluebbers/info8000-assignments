from flask import Flask, escape, request, jsonify, render_template
import sqlite3
import os
import traceback

from io import BytesIO
import base64
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LinearRegression
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from bokeh.io import output_file, show
from bokeh.layouts import column
from bokeh.embed import components
from bokeh.resources import INLINE
from bokeh.util.hex import hexbin
from bokeh.models import ColumnDataSource, HoverTool, PrintfTickFormatter, CategoricalColorMapper,CustomJS
from bokeh.plotting import figure
from bokeh.palettes import Spectral6,RdBu3
from bokeh.transform import factor_cmap, linear_cmap

read_key = '9e895614-6e70-4552-b43a-a058d71bbe4c'
write_key = 'd713b33d-8b18-49a1-931e-f3a47621459b'

#db_location = "/home4/vtsxking/public_html/databases/"
db_location = ""

app = Flask(__name__)

creating = False

@app.route('/')
def fun():
    return "Specify a route you dummy"


def connectToDB(request):
    vers = request.headers.get("version")
    access_code = request.headers.get("access-code")
    if creating:
        time.sleep(.5)
    if vers is not None and access_code is not None:
        c = create_db_if_not_exists(db_location + "echostats_" + str(access_code) + "_" + str(vers) + ".db")
    else:
        c = create_db_if_not_exists(db_location + "echostats_dev_1.5.15.db")
    return c

def getCurrFigAsBase64HTML():
    im_buf_arr = BytesIO()
    plt.gcf().savefig(im_buf_arr,format='png')
    im_buf_arr.seek(0)
    b64data = base64.b64encode(im_buf_arr.read()).decode('utf8')
    return render_template('img.html',img_data=b64data) 

# GET Data

@app.route('/dev')
def testingCalls():
    c = create_db_if_not_exists(db_location + "echostats_dev_1.5.13.db")
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = """
    SELECT * FROM Match
    """
    curr.execute(query)
    matches = [dict(row) for row in curr.fetchall()]

    query = """
    SELECT * FROM MatchPlayer
    """
    curr.execute(query)
    matchPlayers = [dict(row) for row in curr.fetchall()]

    for p in matchPlayers:
        for m in matches:
            if m["session_id"] == p["session_id"] and m["match_time"] == p["match_time"]:
                if "players" not in m:
                    m["players"] = {"orange":[],"blue":[]}
                if p["team_color"] == "orange":
                    m["players"]["orange"].append(p)
                else:
                    m["players"]["blue"].append(p)


    result = matches

    if (len(result) > 0):
        c.close()
        return jsonify(result)
    else:
        c.close()
        return "not found"

@app.route('/vrml_matches')
def vrmlMatchesPage():
    c = create_db_if_not_exists(db_location + "echostats_dev_1.5.15.db")
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = """
    SELECT * FROM Match
    ORDER BY match_time DESC
    LIMIT 50
    """
    curr.execute(query)
    matches = [dict(row) for row in curr.fetchall()]

    query = """
    SELECT * FROM MatchPlayer 
    ORDER BY match_time DESC
    LIMIT 300
    """
    curr.execute(query)
    matchPlayers = [dict(row) for row in curr.fetchall()]

    for m in matches:
        m["players"] = {"orange":[],"blue":[]}

    for p in matchPlayers:
        for m in matches:
            if m["session_id"] == p["session_id"] and m["match_time"] == p["match_time"]:
                if p["team_color"] == "orange":
                    m["players"]["orange"].append(p)
                else:
                    m["players"]["blue"].append(p)

    return render_template('vrml_matches.html', matches=matches)


@app.route('/goal_positions')
def goalPositionsPage():
    c = create_db_if_not_exists(db_location + "goals.db")
    #c = create_db_if_not_exists(db_location + "echostats_dev_1.5.15.db")
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = "SELECT * FROM Goal ORDER BY match_time DESC"
    curr.execute(query)
    matches = [dict(row) for row in curr.fetchall()]

    curr.execute(query)
    goals = [dict(row) for row in curr.fetchall()]


    goalsDF = pd.DataFrame(goals)
    goalsDF = goalsDF[goalsDF["pos_z"] != 0]
    goalsDF = goalsDF[goalsDF["pos_z"] != -27.5]
    goalsDF = goalsDF[goalsDF["pos_z"] != 27.5]

    source = ColumnDataSource(goalsDF)
    feature_names = goalsDF.columns[0:-1].values.tolist()

    # TOOLS="hover,crosshair,pan,wheel_zoom,zoom_in,zoom_out,"

    # p = figure(tools=TOOLS, plot_width=800)

    # p.scatter(source=source, fill_alpha=0.6,
    #         line_color=None)

    backboards = goalsDF[goalsDF["backboard"] == "True"]

    bins1 = hexbin(goalsDF["pos_z"], goalsDF["pos_x"], .4)
    bins2 = hexbin(goalsDF["pos_z"], goalsDF["pos_y"], .4)


    hover1 = HoverTool(tooltips=[
        #('Player Name', '@player_name'),
        #("(z,x)", "(@pos_z, @pos_x)"),
        #("speed", "@disc_speed"),
        ("(z,x)", "($x,$y)"),
    ])

    hover2 = HoverTool(tooltips=[
        #('Player Name', '@player_name'),
        #("(z,x)", "(@pos_z, @pos_x)"),
        #("speed", "@disc_speed"),
        ("(z,x)", "($x,$y)"),
    ])

    TapCallback1 = CustomJS(args=dict(sorc=source), code="""
        console.log('loc: ' + cb_obj.x)
        httpGetAsync("predict_backboard?z="+cb_obj.x+"&x="+cb_obj.y+"", backcall);

        function backcall(text) {
            document.getElementById("backboardorno").innerHTML = "Probability of a backboard shot: " + (text*100).toFixed() + "%";
            console.log(text)
        }

        function httpGetAsync(theUrl, callback)
        {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() { 
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                    callback(xmlHttp.responseText);
            }
            xmlHttp.open("GET", theUrl, true); // true for asynchronous 
            xmlHttp.send(null);
        }
    """)

    TapCallback2 = CustomJS(args=dict(sorc=source), code="""
        console.log('loc: ' + cb_obj.x)
        httpGetAsync("predict_backboard?z="+cb_obj.x+"&y="+cb_obj.y+"", backcall);

        function backcall(text) {
            document.getElementById("backboardorno").innerHTML = "Probability of a backboard shot: " + (text*100).toFixed() + "%";
            console.log(text);
        }

        function httpGetAsync(theUrl, callback)
        {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() { 
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                    callback(xmlHttp.responseText);
            }
            xmlHttp.open("GET", theUrl, true); // true for asynchronous 
            xmlHttp.send(null);
        }
    """)

    #hover = HoverTool(callback=callback)

    #p = figure(tools=[hover,"pan","wheel_zoom"], active_scroll="wheel_zoom", match_aspect=True, background_fill_color='#0b0b11', plot_width=1000)
    p1 = figure(tools=[hover1], match_aspect=True, background_fill_color='#0b0b11', plot_width=1000)
    p2 = figure(tools=[hover2], match_aspect=True, background_fill_color='#0b0b11', plot_width=1000)

    p1.js_on_event('tap', TapCallback1)
    p2.js_on_event('tap', TapCallback2)

    color_mapper = CategoricalColorMapper(factors=["orange", "blue"], palette=[RdBu3[2], RdBu3[0]])

    # p.circle('pos_z', 'pos_x', size=2, fill_alpha=.5, source=source,
    #      color={'field': 'goal_color', 'transform': color_mapper})

    p1.hex_tile(q="q", r="r", size=.4, line_color=None, source=bins1,
        fill_color=linear_cmap('counts', 'Inferno256', 0, max(bins1.counts)))

    p2.hex_tile(q="q", r="r", size=.4, line_color=None, source=bins2,
        fill_color=linear_cmap('counts', 'Inferno256', 0, max(bins2.counts)))

    p1.grid.visible = False
    p1.toolbar.logo = None
    p1.toolbar_location = None
    p1.axis.visible = False
    p1.outline_line_color = None
    p1.background_fill_color = None
    p1.border_fill_color = None

    p2.grid.visible = False
    p2.toolbar.logo = None
    p2.toolbar_location = None
    p2.axis.visible = False
    p2.outline_line_color = None
    p2.background_fill_color = None
    p2.border_fill_color = None


            
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()
		
	# Embed plot into HTML via Flask Render
    v = column(p1, p2)
    script, div = components(v)
    return render_template(
        "goal_positions.html", 
        script=script, 
        div=div,
        js_resources=js_resources,
        css_resources=css_resources,)

@app.route('/predict_backboard')
def predictBackboard():
    in_z = request.args.get('z', 0)
    in_x = request.args.get('x', 0)
    in_y = request.args.get('y', 0)

    c = sqlite3.connect("goals.db")
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = """
    SELECT * FROM Goal
    ORDER BY match_time DESC
    """
    curr.execute(query)
    matches = [dict(row) for row in curr.fetchall()]

    curr.execute(query)
    goals = [dict(row) for row in curr.fetchall()]

    goalsDF = pd.DataFrame(goals)

    goalsDF = goalsDF[goalsDF['goal_pos_x'] < 2]
    goalsDF = goalsDF[goalsDF['goal_pos_x'] > -2]
    goalsDF = goalsDF[goalsDF['goal_pos_y'] < 2]
    goalsDF = goalsDF[goalsDF['goal_pos_y'] > -2]

    # exclude goals where disc position is incorrect
    goalsDF = goalsDF[goalsDF['pos_z'] != 0]

    goalsDF["goal_angle"] = goalsDF["goal_angle"].apply(lambda x: 180 - x if x > 90 else x)
    goalsDF["abs_pos_x"] = goalsDF["pos_x"].abs()
    goalsDF["abs_pos_y"] = goalsDF["pos_y"].abs()
    goalsDF["abs_pos_z"] = goalsDF["pos_z"].abs()
    
    goalsDF["backboard"] = goalsDF["backboard"].replace({"True": 1, "False": 0})
    goalsDF["left_handed"] = goalsDF["left_handed"].replace({"True": 1, "False": 0})
    goalsDF["goal_color"] = goalsDF["goal_color"].replace({"True": 1, "False": 0})

    # PREDICT
    cols = ["abs_pos_x", "abs_pos_y", "abs_pos_z"]
    cols = ["pos_x", "pos_y", "pos_z"]
    X = goalsDF[cols]
    y = goalsDF["backboard"]

    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)

    model = LinearRegression()
    model = KNeighborsClassifier(50)
    model.fit(X,y)

    # create a new row to test
    test = pd.DataFrame([[in_x, in_y, in_z]], columns=cols)

    return str(model.predict_proba(test)[0][1])


@app.route('/player')
def getPlayerData():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    player_name = request.args.get("name")
    if player_name is None:
        return "Must specify `name` of player"
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.cursor()
    query = "SELECT * FROM Player WHERE player_name=?"
    data = (player_name,)
    curr.execute(query,data)
    result = [dict(row) for row in curr.fetchall()]
    if (len(result) > 0):
        c.close()
        return jsonify(result[0])
    else:
        c.close()
        return "Player not found"


@app.route('/match')
def getMatchData():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    session_id = request.args.get("session_id")
    match_time = request.args.get("match_time")
    if session_id is None or match_time is None:
        return "Must specify `session_id` and `match_time`"
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    query = "SELECT * FROM Match WHERE session_id=? AND match_time=?"
    data = (session_id,match_time)
    curr = c.cursor()
    try:
        curr.execute(query,data)
    except:
        return "Get match failed"
    result = [dict(row) for row in curr.fetchall()]
    c.close()
    return jsonify(result)


@app.route('/allMatches')
def getAllMatches():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.execute("SELECT * FROM Match")
    result = [dict(row) for row in curr.fetchall()]
    c.close()
    return jsonify(result)

@app.route('/allPlayerData')
def getAllPlayerData():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    c.row_factory = sqlite3.Row
    curr = c.execute("SELECT * FROM Player")
    result = [dict(row) for row in curr.fetchall()]
    c.close()
    return jsonify(result)

@app.route('/playerCount')
def getPlayerCount():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    curr = c.execute("SELECT COUNT(*) FROM Player")
    out = str(curr.fetchall()[0][0])
    c.close()
    return out

@app.route('/getPlayerList', methods=['GET'])
def getPlayerList():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    curr = c.execute("SELECT player_name FROM Player")
    out = str(curr.fetchall())
    c.close()
    return out

@app.route('/event', methods=['GET'])
def getEvent():
    api_key = request.headers.get('x-api-key')
    if api_key is None or (api_key!=read_key and api_key!=write_key):
        return 'Not authorized'
    c = connectToDB(request)
    session_id = request.args.get("session_id")
    player_name = request.args.get("player_name")
    c.row_factory = sqlite3.Row
    if (session_id is not None):
        curr = c.cursor()
        query = "SELECT * FROM Event WHERE session_id=?"
        data = (session_id,)
        curr.execute(query,data)
        result = [dict(row) for row in curr.fetchall()]
        c.close()
        return jsonify(result)
    elif (player_name is not None):
        curr = c.cursor()
        query = "SELECT * FROM Event WHERE player_name=?"
        data = (player_name,)
        curr.execute(query,data)
        result = [dict(row) for row in curr.fetchall()]
        c.close()
        return jsonify(result)
    else:
        c.close()
        return ''



# Add Data
@app.route('/initDB', methods=['POST'])
def initDB():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    c = connectToDB(request)
    c.close()
    return "DB Initialized"

@app.route("/update_match", methods=['POST'])
def updateMatchWeb():
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    if session_id is None or match_time is None:
        return "Must specify `session_id` and `match_time`"
    c = connectToDB(request)
    query = """
    UPDATE Match
    SET
        blue_team_name = ?,
        orange_team_name = ?,
        round=?
    WHERE
        session_id = ? AND
        match_time = ?

    """
    data = (
        request.values.get('blue_team_name', ""),
        request.values.get('orange_team_name', ""),
        request.values.get('round', 0),
        session_id,
        match_time
        )

    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to add Match"
    c.commit()
    c.close()
    return """<html><head><meta http-equiv="refresh" content="0; URL='https://ignitevr.gg/cgi-bin/EchoStats.cgi/vrml_matches'" /></head><html>"""

@app.route('/addPlayer', methods=['POST'])
def addPlayer():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    if request.values.get('player_name') is None or request.values.get('player_id') is None:
        return "Must specify `player_name` and `player_id`"
    c = connectToDB(request)
    query = "INSERT OR IGNORE INTO Player (player_id, player_name) VALUES(?,?)"
    data = (request.values.get("player_id"), request.values.get("player_name"))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to insert player"
    c.commit()
    c.close()
    return "Player successfully inserted"

@app.route('/accumulatePlayer', methods=['POST'])
def accumulatePlayer():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    if request.values.get('player_name') is None:
        return "Must specify `player_name`"
    c = connectToDB(request)
    query = """
    UPDATE Player 
    SET 
        level = ?, 
        possession_time = possession_time + ?, 
        play_time = play_time + ?, 
        total_assists = total_assists + ?, 
        total_blocks = total_blocks + ?, 
        total_catches = total_catches + ?, 
        total_goals = total_goals + ?,
        total_2_pointers = total_2_pointers + ?,
        total_3_pointers = total_3_pointers + ?,
        total_interceptions = total_interceptions + ?, 
        total_passes = total_passes + ?, 
        total_points = total_points + ?, 
        total_saves = total_saves + ?, 
        total_shots_taken = total_shots_taken + ?, 
        total_steals = total_steals + ?, 
        total_stuns= total_stuns + ?, 
        total_wins = total_wins + ?, 
        game_count= game_count + 1
    WHERE player_name=?"""

    data = (
        request.values.get('level',0),
        request.values.get('possession_time',0),
        request.values.get('play_time',0),
        request.values.get('assists',0),
        request.values.get('blocks',0),
        request.values.get('catches',0),
        request.values.get('goals',0),
        request.values.get('2_pointers',0),
        request.values.get('3_pointers',0),
        request.values.get('interceptions',0),
        request.values.get('passes',0),
        request.values.get('points',0),
        request.values.get('saves',0),
        request.values.get('shots_taken',0),
        request.values.get('steals',0),
        request.values.get('stuns',0),
        request.values.get('wins',0),
        request.values.get('player_name', ""))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to update Player"
    c.commit()
    c.close()
    return "Player updated"


@app.route('/addMatch', methods=['POST'])
def addMatch():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    if session_id is None or match_time is None:
        return "Must specify `session_id` and `match_time`"
    c = connectToDB(request)
    query = """
    REPLACE INTO Match (
        session_id,
        match_time,
        round,
        private,
        client_name,
        blue_team_name,
        orange_team_name,
        blue_team_score,
        orange_team_score,
        winning_team,
        game_clock_start,
        game_clock_end,
        overtime_count,
        finish_reason)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""

    data = (
        session_id,
        match_time,
        request.values.get('round', 0),
        request.values.get('private', False),
        request.values.get('client_name', ""),
        request.values.get('blue_team_name', ""),
        request.values.get('orange_team_name', ""),
        request.values.get('blue_team_score', 0),
        request.values.get('orange_team_score', 0),
        request.values.get('winning_team', ""),
        request.values.get('game_clock_start', None),
        request.values.get('game_clock_end', None),
        request.values.get('overtime_count', 0),
        request.values.get('finish_reason', "not_finished"))

    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to add Match"
    c.commit()
    c.close()
    return "Match added"



@app.route('/addEvent', methods=['POST'])
def addEvent():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    event_type = request.values.get('event_type')
    player_name = request.values.get('player_name')
    player_id = request.values.get('player_id')
    if session_id is None or match_time is None or event_type is None or player_name is None or player_id is None:
        return "Event must specify `session_id` and `match_time` and `event_type` and `player_name` and `player_id`"
    c = connectToDB(request)
    query = """
    INSERT INTO Event (
        session_id,
        match_time, 
        game_clock,
        player_id,
        player_name,
        event_type,
        other_player_id,
        other_player_name,
        pos_x,
        pos_y,
        pos_z,
        x2,
        y2,
        z2)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    """

    data = (
        session_id,
        match_time,
        request.values.get('game_clock', 0),
        player_id,
        player_name,
        event_type,
        request.values.get('other_player_id', None),
        request.values.get('other_player_name', None),
        request.values.get('pos_x', 0),
        request.values.get('pos_y', 0),
        request.values.get('pos_z', 0),
        request.values.get('x2', 0),
        request.values.get('y2', 0),
        request.values.get('z2', 0))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to add Event"
    c.commit()
    c.close()
    return "Event added"


@app.route('/addThrow', methods=['POST'])
def addThrow():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    player_name = request.values.get('player_name')
    player_id = request.values.get('player_id')
    if session_id is None or match_time is None or player_name is None:
        return "Throw must specify `session_id` and `match_time` and `player_name`"
    c = connectToDB(request)
    query = """
    INSERT INTO Throw (
        session_id,
        match_time,
        game_clock,
        player_id,
        player_name,
        pos_x,
        pos_y,
        pos_z,
        vel_x,
        vel_y,
        vel_z,
        left_handed,
        trajectory)
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    """

    data = (
        session_id,
        match_time,
        request.values.get('game_clock', 0),
        player_id,
        player_name,
        request.values.get('pos_x', 0),
        request.values.get('pos_y', 0),
        request.values.get('pos_z', 0),
        request.values.get('vel_x', 0),
        request.values.get('vel_y', 0),
        request.values.get('vel_z', 0),
        request.values.get('left_handed', None),
        request.values.get('trajectory', ""))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to add Throw"
    c.commit()
    c.close()
    return "Throw added"



@app.route('/addGoal', methods=['POST'])
def addGoal():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    point_value = request.values.get('point_value')
    player_id = request.values.get('player_id')
    if session_id is None or match_time is None or point_value is None:
        return "Goal must specify `session_id` and `match_time` and `point_value`"
    c = connectToDB(request)
    query = """
    INSERT INTO Goal (
        session_id,
        match_time,
        game_clock,
        player_id,
        player_name,
        point_value,
        disc_speed,
        goal_distance,
        assist_name,
        goal_type,
        team_scored,
        goal_pos_x,
        goal_pos_y,
        pos_x,
        pos_y,
        pos_z,
        goal_angle,
        backboard,
        goal_color,
        left_handed,
        trajectory)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    """

    data = (
        session_id,
        match_time,
        request.values.get('game_clock',""),
        player_id,
        request.values.get('player_name',""),
        point_value,
        request.values.get('disc_speed',0),
        request.values.get('goal_distance',0),
        request.values.get('assist_name',""),
        request.values.get('goal_type',""),
        request.values.get('team_scored',""),
        request.values.get('goal_pos_x',0),
        request.values.get('goal_pos_y',0),
        request.values.get('pos_x',0),
        request.values.get('pos_y',0),
        request.values.get('pos_z',0),
        request.values.get('goal_angle',0),
        request.values.get('backboard',False),
        request.values.get('goal_color',""),
        request.values.get('left_handed',False),
        request.values.get('trajectory',""))
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to add Goal"
    c.commit()
    c.close()
    return "Goal added"


@app.route('/addMatchPlayer', methods=['POST'])
def addMatchPlayer():
    api_key = request.headers.get('x-api-key')
    if api_key is None or api_key!=write_key:
        return 'Not authorized'
    session_id = request.values.get('session_id')
    match_time = request.values.get('match_time')
    player_name = request.values.get('player_name')
    player_id = request.values.get('player_id')
    if session_id is None or match_time is None or player_name is None:
        return "MatchPlayer must specify `session_id` and `match_time` and `player_name`"
    c = connectToDB(request)
    query = """
    REPLACE INTO MatchPlayer (
        session_id,
        match_time,
        player_id,
        player_name,
        level,
        team_color,
        possession_time,
        play_time,
        points,
        `2_pointers`,
        `3_pointers`,
        shots_taken,
        saves,
        goals,
        stuns,
        passes,
        catches,
        steals,
        blocks,
        interceptions,
        assists,
        average_speed,
        average_speed_lhand,
        average_speed_rhand,
        wingspan,
        playspace_abuses)
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""

    data = (
        session_id,
        match_time,
        player_id,
        player_name,
        request.values.get('level',0),
        request.values.get('team_color',""),
        request.values.get('possession_time',0),
        request.values.get('play_time',0),
        request.values.get('points',0),
        request.values.get('2_pointers',0),
        request.values.get('3_pointers',0),
        request.values.get('shots_taken',0),
        request.values.get('saves',0),
        request.values.get('goals',0),
        request.values.get('stuns',0),
        request.values.get('passes',0),
        request.values.get('catches',0),
        request.values.get('steals',0),
        request.values.get('blocks',0),
        request.values.get('interceptions',0),
        request.values.get('assists',0),
        request.values.get('average_speed',0),
        request.values.get('average_speed_lhand',0),
        request.values.get('average_speed_rhand',0),
        request.values.get('wingspan',0),
        request.values.get('playspace_abuses',0)
        )
    curr = c.cursor()
    try:
        curr.execute(query, data)
    except:
        c.close()
        traceback.print_exc()
        return "Failed to add MatchPlayer"
    c.commit()
    c.close()
    return "MatchPlayer added"


def create_db_if_not_exists(dbname):
    if os.path.exists(dbname):
        return sqlite3.connect(dbname)

    creating = True
    c = sqlite3.connect(dbname)

    try:
        c.executescript('''
        DROP TABLE IF EXISTS Event;
        DROP TABLE IF EXISTS Goal;
        DROP TABLE IF EXISTS MatchPlayer;
        DROP TABLE IF EXISTS Match;
        DROP TABLE IF EXISTS Player;
        DROP TABLE IF EXISTS Throw;
        DROP TABLE IF EXISTS VRMLPlayer;
        ''')
        
        #CREATE DATABASE
        c.executescript('''
        CREATE TABLE `Event` (
        `session_id` TEXT NOT NULL,
        `match_time` datetime NOT NULL,
        `game_clock` FLOAT NOT NULL,
        `player_id` INTEGER NOT NULL,
        `player_name` TEXT NOT NULL,
        `event_type` TEXT NOT NULL,
        `other_player_id` INTEGER DEFAULT NULL,
        `other_player_name` TEXT DEFAULT NULL,
        `pos_x` FLOAT NOT NULL,
        `pos_y` FLOAT NOT NULL,
        `pos_z` FLOAT NOT NULL,
        `x2` FLOAT,
        `y2` FLOAT,
        `z2` FLOAT,
        FOREIGN KEY (session_id, match_time) 
            REFERENCES Match (session_id, match_time),
        FOREIGN KEY (player_id) 
            REFERENCES Player (player_id)
        );

        CREATE TABLE `Throw` (
        `session_id` TEXT NOT NULL,
        `match_time` datetime NOT NULL,
        `game_clock` FLOAT NOT NULL,
        `player_id` INTEGER NOT NULL,
        `player_name` TEXT NOT NULL,
        `pos_x` FLOAT NOT NULL,
        `pos_y` FLOAT NOT NULL,
        `pos_z` FLOAT NOT NULL,
        `vel_x` FLOAT NOT NULL,
        `vel_y` FLOAT NOT NULL,
        `vel_z` FLOAT NOT NULL,
        `left_handed` boolean,
        `trajectory` TEXT NOT NULL,
        FOREIGN KEY (session_id, match_time) 
            REFERENCES Match (session_id, match_time),
        FOREIGN KEY (player_id) 
            REFERENCES Player (player_id)
        );

        CREATE TABLE `Goal` (
        `session_id` TEXT NOT NULL,
        `match_time` datetime NOT NULL,
        `game_clock` FLOAT NOT NULL,
        `player_id` INTEGER NOT NULL,
        `player_name` TEXT NOT NULL,
        `point_value` INTEGER NOT NULL,
        `disc_speed` FLOAT NOT NULL,
        `goal_distance` FLOAT NOT NULL,
        `assist_name` TEXT DEFAULT NULL,
        `goal_type` TEXT NOT NULL,
        `team_scored` TEXT NOT NULL,
        `goal_pos_x` FLOAT NOT NULL,
        `goal_pos_y` FLOAT NOT NULL,
        `pos_x` FLOAT NOT NULL,
        `pos_y` FLOAT NOT NULL,
        `pos_z` FLOAT NOT NULL,
        `goal_angle` FLOAT NOT NULL,
        `backboard` BOOLEAN NOT NULL,
        `goal_color` TEXT NOT NULL,
        `left_handed` BOOLEAN NOT NULL,
        `trajectory` TEXT,
        FOREIGN KEY (session_id, match_time) 
            REFERENCES Match (session_id, match_time),
        FOREIGN KEY (player_id) 
            REFERENCES Player (player_id),
        UNIQUE (session_id, match_time, game_clock)
        );

        CREATE TABLE `Match` (
        `session_id` TEXT NOT NULL,
        `match_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `round` INTEGER NOT NULL DEFAULT '0',
        `private` BOOLEAN NOT NULL DEFAULT False,
        `client_name` TEXT NOT NULL,
        `blue_team_name` TEXT NOT NULL,
        `orange_team_name` TEXT NOT NULL,
        `blue_team_score` INTEGER NOT NULL DEFAULT '0',
        `orange_team_score` INTEGER NOT NULL DEFAULT '0',
        `winning_team` TEXT NOT NULL,
        `game_clock_start` FLOAT,
        `game_clock_end` FLOAT,
        `overtime_count` INTEGER,
        `finish_reason` TEXT,
        PRIMARY KEY(session_id, match_time)
        );

        CREATE TABLE `MatchPlayer` (
        `session_id` TEXT NOT NULL,
        `match_time` datetime NOT NULL,
        `player_id` INTEGER NOT NULL,
        `player_name` TEXT NOT NULL,
        `level` INTEGER NOT NULL DEFAULT '0',
        `team_color` TEXT NOT NULL,
        `possession_time` FLOAT NOT NULL DEFAULT '0',
        `play_time` FLOAT NOT NULL DEFAULT '0',
        `points` INTEGER NOT NULL DEFAULT '0',
        `2_pointers` INTEGER NOT NULL DEFAULT '0',
        `3_pointers` INTEGER NOT NULL DEFAULT '0',
        `shots_taken` INTEGER NOT NULL DEFAULT '0',
        `saves` INTEGER NOT NULL DEFAULT '0',
        `goals` INTEGER NOT NULL DEFAULT '0',
        `stuns` INTEGER NOT NULL DEFAULT '0',
        `passes` INTEGER NOT NULL DEFAULT '0',
        `catches` INTEGER NOT NULL DEFAULT '0',
        `steals` INTEGER NOT NULL DEFAULT '0',
        `blocks` INTEGER NOT NULL DEFAULT '0',
        `interceptions` INTEGER NOT NULL DEFAULT '0',
        `assists` INTEGER NOT NULL DEFAULT '0',
        `average_speed` FLOAT NOT NULL DEFAULT '0',
        `average_speed_lhand` FLOAT NOT NULL DEFAULT '0',
        `average_speed_rhand` FLOAT NOT NULL DEFAULT '0',
        `wingspan` FLOAT NOT NULL DEFAULT '0',
        `playspace_abuses` INTEGER NOT NULL DEFAULT '0',
        FOREIGN KEY (session_id, match_time) 
            REFERENCES Match (session_id, match_time),
        FOREIGN KEY (player_id) 
            REFERENCES Player (player_id),
        UNIQUE (session_id, match_time, player_id)
        );

        CREATE TABLE `Player` (
        `player_id` INTEGER NOT NULL,
        `profile_page_link` TEXT NOT NULL DEFAULT '',
        `profile_link` TEXT NOT NULL DEFAULT 'https://ignitevr.gg/wp-content/uploads/2019/04/defualt_profile_14_4k.jpg',
        `player_name` TEXT UNIQUE NOT NULL DEFAULT '???',
        `level` INTEGER NOT NULL DEFAULT '0',
        `possession_time` FLOAT NOT NULL DEFAULT '0',
        `play_time` FLOAT NOT NULL DEFAULT '0',
        `total_points` INTEGER NOT NULL DEFAULT '0',
        `total_shots_taken` INTEGER NOT NULL DEFAULT '0',
        `total_saves` INTEGER NOT NULL DEFAULT '0',
        `total_goals` INTEGER NOT NULL DEFAULT '0',
        `total_2_pointers` INTEGER NOT NULL DEFAULT '0',
        `total_3_pointers` INTEGER NOT NULL DEFAULT '0',
        `total_stuns` INTEGER NOT NULL DEFAULT '0',
        `total_passes` INTEGER NOT NULL DEFAULT '0',
        `total_catches` INTEGER NOT NULL DEFAULT '0',
        `total_steals` INTEGER NOT NULL DEFAULT '0',
        `total_blocks` INTEGER NOT NULL DEFAULT '0',
        `total_interceptions` INTEGER NOT NULL DEFAULT '0',
        `total_assists` INTEGER NOT NULL DEFAULT '0',
        `total_wins` INTEGER NOT NULL DEFAULT '0',
        `game_count` INTEGER NOT NULL DEFAULT '0',
        PRIMARY KEY (player_id)
        );


        CREATE TABLE `VRMLPlayer` (
        `player_name` TEXT UNIQUE NOT NULL,
        `player_page` TEXT NOT NULL,
        `player_logo` TEXT NOT NULL,
        `team_name` TEXT NOT NULL,
        `team_page` TEXT NOT NULL,
        `team_logo` TEXT NOT NULL,
        FOREIGN KEY (player_name) REFERENCES Player (player_name)
        );
        ''')
        
        
        
        c.executescript("PRAGMA foreign_keys = ON;")
            
        
    except sqlite3.Error as e:
        c.close()
        print(e)
    
    creating = False

    return c