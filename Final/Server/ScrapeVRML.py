﻿import requests
import re
import sqlite3

VRML_URL = "https://vrmasterleague.com/EchoArena/Players/List/"

def findString(input, beginIndex, beginStr, endStr):
        begin = page[i].find(str(beginStr), beginIndex)
        end = page[i].find(str(endStr), begin)
        return str(page[i][begin + len(beginStr) : end])

page = [str(requests.get(VRML_URL).content)]

# only supports 3-digit player counts
numPlayersLoc = page[0].find('header-count">There are currently ')
numPlayers = int(page[0][numPlayersLoc+34:numPlayersLoc+37])
print(numPlayers)

for i in range(1,int(numPlayers/100)+1):
    page.append(str(requests.get(str(VRML_URL+"?posMin="+str(i*100+1))).content))

for i in range(0,len(page)):
    playerIndices = [m.start() for m in re.finditer('<td class="player_cell">', page[i])]
    for j in range(0,len(playerIndices)):

        # Player name
        playerName = findString(page[i], playerIndices[j], "<span>", "</span>")

        # player page URL
        playerURL = 'https://vrmasterleague.com' + findString(page[i], playerIndices[j], '<a href="', '">')

        # team name
        teamName = findString(page[i], playerIndices[j], 'team_name">', '</span>')

        # team page URL
        teamURL = 'https://vrmasterleague.com' + findString(page[i], playerIndices[j], 'team_cell"><a href="', '" class')

        # player logo
        playerLogo = 'https://vrmasterleague.com' + findString(page[i], playerIndices[j], '"><img src="', '" class="player_logo"')

        # team logo
        teamLogo = 'https://vrmasterleague.com' + findString(page[i], playerIndices[j], '" class="team_link"><img src="', '" class="team_logo"')

        print(playerName + "\t" + teamName + "\t" + playerURL+ "\t" + teamURL + "\t" + playerLogo + "\t" + teamLogo)


        # Insert into DB
        c = sqlite3.connect("echostats_dev_1.5.13.db")

        query = "REPLACE INTO VRMLPlayer (player_name, player_page, player_logo, team_name, team_page, team_logo) VALUES(?,?,?,?,?,?)"
        data = (
            playerName,
            playerURL,
            playerLogo,
            teamName,
            teamURL,
            teamLogo
            )
        curr = c.cursor()
        #try:
        curr.execute(query, data)
        #except:
        #    c.close()
        #    print("Failed to insert player")
        c.commit()
        c.close()
