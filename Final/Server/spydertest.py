from flask import Flask, escape, request, jsonify, render_template
import sqlite3
import os
import traceback

#"""
from io import BytesIO
import base64
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LinearRegression
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
import sklearn.metrics as metrics
import seaborn as sns
sns.set()

#c = sqlite3.connect("echostats_dev_1.5.16.db")
c = sqlite3.connect("goals.db")
c.row_factory = sqlite3.Row
curr = c.cursor()
query = """
SELECT * FROM Goal
ORDER BY match_time DESC
"""
curr.execute(query)
matches = [dict(row) for row in curr.fetchall()]

curr.execute(query)
goals = [dict(row) for row in curr.fetchall()]

goalsDF = pd.DataFrame(goals)

goalsDF = goalsDF[goalsDF['goal_pos_x'] < 2]
goalsDF = goalsDF[goalsDF['goal_pos_x'] > -2]
goalsDF = goalsDF[goalsDF['goal_pos_y'] < 2]
goalsDF = goalsDF[goalsDF['goal_pos_y'] > -2]

# exclude goals where disc position is incorrect
goalsDF = goalsDF[goalsDF['pos_z'] != 0]


goalsDF["goal_angle"] = goalsDF["goal_angle"].apply(lambda x: 180 - x if x > 90 else x)
goalsDF["abs_pos_x"] = goalsDF["pos_x"].abs()
goalsDF["abs_pos_y"] = goalsDF["pos_y"].abs()
goalsDF["abs_pos_z"] = goalsDF["pos_z"].abs()

#sns.pairplot(goalsDF)
#sns.kdeplot(goalsDF["goal_pos_x"], goalsDF["goal_pos_y"], shade=True, height=10)
#sns.scatterplot(data=goalsDF, x="goal_pos_x", y="goal_pos_y", alpha=.1, hue="point_value", size="goal_distance")
#sns.distplot(goalsDF["goal_angle"])
#sns.distplot(goalsDF["goal_distance"])
#sns.scatterplot(data=goalsDF, x="goal_distance", y="disc_speed", alpha=.5, hue="point_value", palette="muted")
#sns.jointplot(data=goalsDF, x="goal_pos_x", y="goal_pos_y", kind="hex", height=10)      # all goals
sns.jointplot(data=goalsDF[goalsDF["backboard"] == "True"], x="goal_pos_x", y="goal_pos_y", kind="hex", height=10)      # only backboards
#sns.jointplot(data=goalsDF[goalsDF['goal_angle'] > 0.1], x="goal_distance", y="goal_angle", kind="reg", height=10)
#sns.jointplot(data=goalsDF[abs(goalsDF['pos_x']) > 0], x="goal_pos_x", y="goal_angle", kind="reg", height=10)
#sns.scatterplot(data=goalsDF, x="pos_x", y="goal_pos_x", alpha=.5, hue="point_value", palette="muted")
#sns.scatterplot(data=goalsDF, x="pos_z", y="pos_x", alpha=.5, size=1, hue="backboard", palette="muted")
#sns.violinplot(data=goalsDF, x="abs_pos_z", y="backboard")

goalsDF["backboard"] = goalsDF["backboard"].replace({"True": 1, "False": 0})
goalsDF["left_handed"] = goalsDF["left_handed"].replace({"True": 1, "False": 0})
goalsDF["goal_color"] = goalsDF["goal_color"].replace({"True": 1, "False": 0})
p_goals = goalsDF.groupby(["player_name"]).mean()
p_goals["count"] = goalsDF.groupby(["player_name"]).count()["goal_type"]

p_goals = p_goals.reset_index()

display(p_goals[p_goals["player_name"] == "VTSxKING"])

p_goals = p_goals[p_goals["goal_angle"] > 10]
p_goals = p_goals[p_goals["count"] > 10]


p_goals.sort_values(by="count", inplace=True)
print(p_goals["player_name"])

#g = sns.catplot(data=p_goals, x="player_name", y="goal_angle", kind="bar", palette="muted", height=10, aspect=5)
#g.set_xticklabels(rotation=90)









# TRAINING

# Split data in train and test data
np.random.seed(0)
indices = np.random.permutation(len(p_goals))

#X = np.reshape(p_goals.pos_x, (1,len(p_goals)))
#y = np.reshape(p_goals.goal_pos_x, (1,len(p_goals)))

print(goalsDF.columns)
goalsDF = goalsDF[goalsDF["pos_x"] != 0]
goalsDF = goalsDF[goalsDF["point_value"] == 2]
#goalsDF = goalsDF[goalsDF["player_name"] == "Saloona22"]    # filter only a single player
print(goalsDF["backboard"].mean())
X = goalsDF[["abs_pos_x", "abs_pos_y", "abs_pos_z"]]
y = goalsDF["backboard"]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)

model = LinearRegression()
model.fit(X_train,y_train)
ypredict = model.predict(X_test)
score = model.score(X_test, y_test)

print("Predict: " + str(ypredict))
print("Score: " + str(score))

# Create and fit a nearest-neighbor classifier
scores= {}
scores_list = []
for k in range(5,10):
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    y_pred = knn.predict(X_test)
    scores[k] = metrics.accuracy_score(y_test,y_pred)
    scores_list.append(metrics.accuracy_score(y_test, y_pred))
    
print("Scores: " + str(scores))
print("Scores_list: " + str(scores_list))





